webpackJsonp([2],{

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestaurantsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_call_number__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__landing_landing__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_category_category__ = __webpack_require__(79);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the RestaurantsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RestaurantsPage = (function () {
    function RestaurantsPage(navCtrl, navParams, call, resData) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.call = call;
        this.resData = resData;
        this.selectedItems = [];
        this.showDetailsView = false;
        this.restaurants = [];
        this.matched = 0;
        this.matchedIngredients = [];
        this.selectedItems = navParams.get('selectedItems');
    }
    RestaurantsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.selectedItems = this.navParams.get('selectedItems') ? this.navParams.get('selectedItems') : '';
        this.resData.getRestaurants(this.selectedItems).subscribe((function (res) {
            if (res.data.length) {
                _this.restaurants = res.data;
                if (_this.selectedItems.length) {
                    _this.restaurants.forEach(function (element, key) {
                        _this.selectedItems.forEach(function (ingredient) {
                            if (element.ingredients.includes(ingredient.annotation)) {
                                _this.matched += 1;
                            }
                        });
                        _this.restaurants[key].matched = Math.round((_this.matched / _this.selectedItems.length) * 100);
                        _this.matched = 0;
                    });
                }
            }
            else {
                _this.restaurants = [];
            }
            console.log(res.data);
        }));
    };
    RestaurantsPage.prototype.showSeleted = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__landing_landing__["a" /* LandingPage */], { 'selectedItems': this.selectedItems });
    };
    RestaurantsPage.prototype.removeItem = function (index) {
        this.selectedItems.splice(index, 1);
        this.ionViewDidLoad();
    };
    RestaurantsPage.prototype.showDetails = function (rest) {
        this.showDetailsView = true;
        this.selectedRestaurant = rest;
        this.matchedIngredients = this.selectedRestaurant.ingredients.split(',');
        this.selectedRestaurant.ingredients = this.matchedIngredients;
    };
    RestaurantsPage.prototype.showRestaurants = function () {
        this.showDetailsView = false;
    };
    RestaurantsPage.prototype.makeCall = function () {
        this.call.callNumber("919030070213", true)
            .then(function () { return console.log('Launched dialer!'); })
            .catch(function () { return console.log('Error launching dialer'); });
    };
    RestaurantsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-restaurants',template:/*ion-inline-start:"/Users/imran/Documents/ionic/foodifine/src/pages/restaurants/restaurants.html"*/'\n\n\n\n\n<ion-content padding>\n	\n    <ion-toolbar>\n		<ion-buttons left clear> <ion-icon name="menu"></ion-icon></ion-buttons>\n		\n    	<ion-title medel><img src="assets/imgs/logo.png" ></ion-title>\n    	<ion-buttons right (click)="showSeleted()">\n   		   <ion-icon  name="ios-funnel-outline"><div class="funnel-count">{{selectedItems.length}}</div></ion-icon>	\n    	</ion-buttons>\n 	</ion-toolbar>\n <div class="add-sce" *ngIf="selectedItems.length>0">\n 	<ion-item no-lines>\n      <ion-scroll scrollX="true">\n        <div class="ui button" *ngFor="let i of selectedItems;let x=\'index\' ">{{i.annotation}}\n			<ion-icon (click)="removeItem(x)" right ios="ios-close" md="md-close"></ion-icon>\n		</div>\n      </ion-scroll>\n    </ion-item>	\n</div>\n 	<div class="custom-search">\n	 	<ion-searchbar\n		  [(ngModel)]="myInput" [disabled]=disabled\n		  [showCancelButton]="shouldShowCancel"\n		  (ionCancel)="onCancel($event)">\n		  \n		</ion-searchbar>   \n	</div>\n	<div *ngIf="!showDetailsView">\n		<div class="restaurant-box" *ngFor="let res of restaurants">\n			<ion-grid>\n				<ion-row (click)="showDetails(res)">\n					<ion-col col-4 class="items-imgs">\n						<img [src]="res.image_url"></ion-col>\n					<ion-col col-8 class="restaurant-bg">\n							<h3>{{res.restaurant}}</h3>\n							<div class="restaurant-pading">\n						<ion-col col-3 class="restaurant-1Mile" *ngIf="res.matched">{{res.matched}}%</ion-col>\n						<ion-col col-3 class="restaurant-1Mile" *ngIf="!res.matched">0%</ion-col>\n						<ion-col col-3 class="restaurant-1Mile">1Mile</ion-col>\n						<ion-col col-3 class="restaurant-icon-3"><img src="assets/simages/star-and-4.5-icon.png"></ion-col>\n						<ion-col col-3 class="restaurant-icon-3"><img src="assets/simages/icon-3.png"></ion-col>\n					</div>\n\n					</ion-col>\n				</ion-row>		\n			</ion-grid>	\n		</div>	\n	</div>\n	<div class="restaurant-detail-view" *ngIf="showDetailsView">\n		<div class="show-rest-Img" [ngStyle]="{\'background-image\': \'url(\' + selectedRestaurant.image_url + \')\'}">\n			<ion-row>\n				<ion-col col-6 left >\n					<div (click)="showRestaurants()" class="back-arrow" ><img src="assets/simages/left-arrow.png"></div>\n				</ion-col>\n				<ion-col col-6 right><div class="certified-logo"><img src="assets/simages/certified-logo.png"></div></ion-col>\n			</ion-row>\n		</div>\n		<div class="restaurant-detail">\n			<ion-row>\n				<ion-col col-6 left>\n					<h2>{{selectedRestaurant.restaurant}}</h2>				\n				</ion-col>\n				<ion-col col-2 left>\n					<h2>{{selectedRestaurant.matched}}%</h2>				\n				</ion-col>\n				<ion-col col-2 left>\n					1Mile		\n				</ion-col>\n				<ion-col col-2 right>\n					<img src="assets/simages/star-icon.png">			\n				</ion-col>\n			\n			\n				<ion-col >\n					<div class="makeit-links">\n							<a (click)="makeCall()"><img src="assets/simages/call-icon.png"></a><a href="#"><span>Make it Favorite </span></a> <a href="#"><span class="i-dont"> I don\'t like this </span></a>\n					</div>\n				</ion-col>\n			</ion-row>\n			\n			<div class="rest-content">\n				<ion-row>\n				<ion-col >\n					<ul>\n							<li class="item" *ngFor="let ingredient of selectedRestaurant.ingredients">{{ingredient}}</li>\n							\n					\n					</ul>\n				</ion-col>\n				\n			</ion-row>\n			</div>\n		</div>\n	</div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/imran/Documents/ionic/foodifine/src/pages/restaurants/restaurants.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_call_number__["a" /* CallNumber */], __WEBPACK_IMPORTED_MODULE_4__providers_category_category__["a" /* CategoryProvider */]])
    ], RestaurantsPage);
    return RestaurantsPage;
}());

//# sourceMappingURL=restaurants.js.map

/***/ }),

/***/ 114:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 114;

/***/ }),

/***/ 156:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/landing/landing.module": [
		282,
		1
	],
	"../pages/restaurants/restaurants.module": [
		283,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 156;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__landing_landing__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_device__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_deviceinfo_deviceinfo__ = __webpack_require__(205);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = (function () {
    function HomePage(navCtrl, device, deviceInfo) {
        this.navCtrl = navCtrl;
        this.device = device;
        this.deviceInfo = deviceInfo;
        this.slider = [];
        this.slider = [
            {
                src: "assets/simages/swiper-img-1.png",
                id: "1",
                title: "Ingredient Selection",
                desc: "Choose Ingredients you want to eat"
            },
            {
                src: "assets/simages/swiper-img-2.png",
                id: "2",
                title: "Location:",
                desc: "Activate your location"
            },
            {
                src: "assets/simages/swiper-img-3.png",
                id: "2",
                title: "Ingredients Search",
                desc: "We find the match of receipes with ingredients you choose."
            },
            {
                src: "assets/simages/swiper-img-3.png",
                id: "2",
                title: "Call Resturant",
                desc: "Call the resturant and book a table!!"
            }
        ];
        this.deviceInfo.saveDeviceId(this.device.uuid).subscribe(function (res) { console.log(res); });
    }
    HomePage.prototype.goLanding = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__landing_landing__["a" /* LandingPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/imran/Documents/ionic/foodifine/src/pages/home/home.html"*/'\n\n<ion-content padding>\n	<div class="slider-header header-sce">\n			<div ><img src="assets/imgs/header-logo.png" ></div>\n		\n	</div>\n	<div class="slider-section">\n		<ion-slides pager="true" autoplay="3000" loop="true" speed="1500">\n		    <ion-slide *ngFor="let slide of slider" class="swiper-sce">\n		      <img [src]="slide.src" class="slide-image"/>\n		      <h1 [innerText]="slide.title"></h1>\n		      <p>{{slide.desc}}</p>\n		    </ion-slide>\n		</ion-slides>\n	</div>\n	<div class="get-started">\n		<button ion-button color="secondary" (click)="goLanding()">Get Started</button>\n	</div>\n</ion-content>\n'/*ion-inline-end:"/Users/imran/Documents/ionic/foodifine/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_device__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_4__providers_deviceinfo_deviceinfo__["a" /* DeviceinfoProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceinfoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the DeviceinfoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var DeviceinfoProvider = (function () {
    function DeviceinfoProvider(_http) {
        this._http = _http;
        this.url = "http://acenblu.com/foodifine/saveDevice.php";
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
        this.body = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["e" /* URLSearchParams */]();
        this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: this.headers });
        this.body.set('uid', 'I like to eat delicious tacos. Only cheeseburger with cheddar are better than that. But then again, pizza with pepperoni, mushrooms, and tomatoes is so good!');
    }
    DeviceinfoProvider.prototype.saveDeviceId = function (uid) {
        return this._http.post(this.url, { 'uid': uid }, this.options).
            map(function (response) { return response.json(); });
    };
    DeviceinfoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]])
    ], DeviceinfoProvider);
    return DeviceinfoProvider;
}());

//# sourceMappingURL=deviceinfo.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(230);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 230:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_call_number__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_device__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_home_home__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_landing_landing__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_category_category__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_restaurants_restaurants__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pipes_filter_filter__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_deviceinfo_deviceinfo__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_native_geocoder__ = __webpack_require__(161);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_15__pipes_filter_filter__["a" /* FilterPipe */],
                __WEBPACK_IMPORTED_MODULE_12__pages_landing_landing__["a" /* LandingPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_restaurants_restaurants__["a" /* RestaurantsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/landing/landing.module#LandingPageModule', name: 'LandingPage', segment: 'landing', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/restaurants/restaurants.module#RestaurantsPageModule', name: 'RestaurantsPage', segment: 'restaurants', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_landing_landing__["a" /* LandingPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_restaurants_restaurants__["a" /* RestaurantsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_device__["a" /* Device */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_call_number__["a" /* CallNumber */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_13__providers_category_category__["a" /* CategoryProvider */],
                __WEBPACK_IMPORTED_MODULE_16__providers_deviceinfo_deviceinfo__["a" /* DeviceinfoProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(204);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/imran/Documents/ionic/foodifine/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/imran/Documents/ionic/foodifine/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FilterPipe = (function () {
    function FilterPipe() {
    }
    FilterPipe.prototype.transform = function (items, searchText) {
        if (searchText == null)
            return items;
        return items.filter(function (item) {
            return item.annotation.toLowerCase().indexOf(searchText) > -1;
        });
    };
    FilterPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'filter'
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], FilterPipe);
    return FilterPipe;
}());

//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LandingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_category_category__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__restaurants_restaurants__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_geocoder__ = __webpack_require__(161);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var LandingPage = (function () {
    function LandingPage(platform, navCtrl, navParams, getData, geo, nativeGeocoder, alertMessage, loading) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.getData = getData;
        this.geo = geo;
        this.nativeGeocoder = nativeGeocoder;
        this.alertMessage = alertMessage;
        this.loading = loading;
        this.items = [];
        this.count = 0;
        this.selectedItems = [];
        this.tempItems = [];
        this.showSelected = false;
        this.searchText = '';
        this.nextPageData = [];
        this.geocoder = new google.maps.Geocoder();
        platform.ready().then(function () {
            // get current position
            geo.getCurrentPosition().then(function (pos) {
                console.log(pos, 'lon: ' + pos.coords.longitude);
            });
            var watch = geo.watchPosition().subscribe(function (pos) {
                console.log('lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude);
            });
            // to stop watching
            watch.unsubscribe();
        });
    }
    LandingPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.selectedItems = this.navParams.get('selectedItems') ? this.navParams.get('selectedItems') : [];
        if (this.selectedItems.length > 0)
            this.showSelectedItems();
        else {
            this.getData.getCategories(this.count).subscribe((function (res) {
                _this.items = res.data;
                _this.count = _this.items.length;
                // this.nativeGeocoder.forwardGeocode('Berlin')
                //   .then((coordinates: NativeGeocoderForwardResult) => console.log('The coordinates are latitude=' + coordinates.latitude + ' and longitude=' + coordinates.longitude))
                //   .catch((error: any) => console.log(error));
                //  this.loadMap();
            }));
            this.nativeGeocoder.reverseGeocode(17.4530129, 78.4327117)
                .then(function (result) { return console.log(JSON.stringify(result)); })
                .catch(function (error) { return console.log(error); });
        }
    };
    LandingPage.prototype.selectSearchResult = function (item) {
        var _this = this;
        this.geocoder.geocode({ 'placeId': item.place_id }, function (results, status) {
            if (status === 'OK' && results[0]) {
                _this.geocoder.nearbySearch({
                    location: results[0].geometry.location,
                    radius: '500',
                    types: ['restaurant'],
                    key: 'AIzaSyCF6fsUcBfUr_RVeXJVycUhUURF_Vrhk3g'
                }, function (near_places) {
                    _this.zone.run(function () {
                        _this.nearbyItems = [];
                        for (var i = 0; i < near_places.length; i++) {
                            _this.nearbyItems.push(near_places[i]);
                        }
                    });
                });
            }
        });
        console.log(this.nearbyItems);
    };
    LandingPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        this.getData.getCategories(this.count).subscribe((function (res) {
            if (res.data) {
                _this.nextPageData = res.data;
                res.data.forEach(function (element) {
                    _this.items.push(element);
                });
            }
            _this.count = _this.items.length;
            infiniteScroll.complete();
        }));
    };
    LandingPage.prototype.findMyLocation = function () {
        var _this = this;
        if (this.selectedItems.length == 0) {
            var alert_1 = this.alertMessage.create({
                subTitle: 'Please select at least one ingredient',
                buttons: ['OK']
            });
            alert_1.present();
            // let geoOptions = {enableHighAccuracy : true, timeout: 5000,
            //   maximumAge: 0};
            // this.currentPos = navigator.geolocation.getCurrentPosition(this.success,this.error,geoOptions);
            return;
        }
        var geoOptions = { enableHighAccuracy: true, timeout: 5000,
            maximumAge: 0 };
        try {
            this.watchId = this.geo.watchPosition(geoOptions).subscribe(function (data) {
                _this.myLocation = data.coords.latitude;
            }, function (error) {
                _this.message = "GPS error ";
            });
        }
        catch (err) {
            alert('error ' + err);
            this.message = "error";
        }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__restaurants_restaurants__["a" /* RestaurantsPage */], { 'selectedItems': this.selectedItems });
    };
    LandingPage.prototype.success = function (pos) {
        var crd = pos.coords;
        console.log('Your current position is:');
        console.log("Latitude : " + crd.latitude);
        console.log("Longitude: " + crd.longitude);
        console.log("More or less " + crd.accuracy + " meters.");
    };
    LandingPage.prototype.error = function () {
        console.log("error");
    };
    LandingPage.prototype.showSelectedItems = function () {
        if (this.selectedItems.length > 0) {
            if (this.showSelected) {
                this.showSelected = false;
                this.items = this.tempItems;
            }
            else {
                this.tempItems = this.items;
                this.items = this.selectedItems;
                this.showSelected = true;
            }
        }
    };
    LandingPage.prototype.selectItem = function (item) {
        if (item.selected) {
            item.selected = false;
            var index = this.selectedItems.findIndex(function (x) { return x.title === item.title; });
            this.selectedItems.splice(index, 1);
        }
        else {
            item.selected = true;
            this.selectedItems.push(item);
        }
    };
    LandingPage.prototype.search = function (event) {
        var _this = this;
        if (this.searchText.length > 2) {
            this.getData.getSearchdata(this.searchText).subscribe((function (res) {
                _this.items = res.data;
                console.log(res.data);
            }));
        }
    };
    LandingPage.prototype.skipSelection = function () {
        this.selectedItems = [];
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__restaurants_restaurants__["a" /* RestaurantsPage */], { 'selectedItems': this.selectedItems });
    };
    LandingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-landing',template:/*ion-inline-start:"/Users/imran/Documents/ionic/foodifine/src/pages/landing/landing.html"*/'\n<ion-content padding>\n	<ion-grid>\n  <ion-row class="header-bar-sce">\n			<ion-col  col-1 class="header-back-arrow" ><img src="assets/simages/left-arrow.png" alt=""></ion-col>\n    <ion-col col-9>\n    <ion-searchbar [(ngModel)]="searchText" (ngModelChange)="search($event)">\n		</ion-searchbar>\n		 \n	</ion-col>\n    <ion-col  col-2 class="selected-items" > \n    	<span (click)="showSelectedItems()" class="item-count" [ngClass]="{\'active-class\': showSelected}" \n    	[innerText]="selectedItems.length"></span></ion-col>\n  </ion-row>\n</ion-grid>\n<ion-grid>\n	<ion-row >\n		<ion-col col-6 *ngFor="let item of items"   class="" (click)="selectItem(item)">\n		<ion-card class="img-box">\n		    <img [src]="item.image"/>\n		    	<span class="tick-mark"><ion-icon *ngIf="item.selected" ios="ios-checkbox" md="md-checkbox"></ion-icon></span>\n		    <div class="item-names" [innerText]="item.annotation"></div>\n 		 </ion-card>\n		</ion-col>\n		\n	</ion-row>\n</ion-grid>\n<ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n	<ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data..."></ion-infinite-scroll-content>\n</ion-infinite-scroll>\n\n\n</ion-content>\n<ion-footer>\n  <ion-toolbar>\n\n     <button ion-button left color="#01bcd4" (click)="findMyLocation()">FIND MY DISH</button>\n\n    <ion-buttons right>\n      \n       <button ion-button clear (click)="skipSelection()">SKIP</button>\n\n    </ion-buttons>\n\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/imran/Documents/ionic/foodifine/src/pages/landing/landing.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_category_category__["a" /* CategoryProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_geocoder__["a" /* NativeGeocoder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], LandingPage);
    return LandingPage;
}());

;
//# sourceMappingURL=landing.js.map

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CategoryProvider = (function () {
    function CategoryProvider(_http) {
        this._http = _http;
        this.url = "http://acenblu.com/foodifine/index.php";
        this.searchUrl = "http://acenblu.com/foodifine/getSearchData.php";
        this.getRestaurantsUrl = "http://acenblu.com/foodifine/getRestaurants.php";
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
        this.body = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["e" /* URLSearchParams */]();
        this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: this.headers });
    }
    CategoryProvider.prototype.getCategories = function (count) {
        return this._http.get(this.url + '?count=' + count).
            map(function (response) { return response.json(); });
    };
    CategoryProvider.prototype.getSearchdata = function (searchText) {
        return this._http.get(this.searchUrl + '?searchText=' + searchText).
            map(function (response) { return response.json(); });
    };
    CategoryProvider.prototype.getRestaurants = function (postData) {
        return this._http.post(this.getRestaurantsUrl, postData, this.options).
            map(function (response) { return response.json(); });
    };
    CategoryProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]])
    ], CategoryProvider);
    return CategoryProvider;
}());

//# sourceMappingURL=category.js.map

/***/ })

},[206]);
//# sourceMappingURL=main.js.map