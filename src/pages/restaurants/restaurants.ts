import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { LandingPage } from '../landing/landing';
import { CategoryProvider } from '../../providers/category/category';

/**
 * Generated class for the RestaurantsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-restaurants',
  templateUrl: 'restaurants.html',
})
export class RestaurantsPage {
	selectedItems : any = [];
	showDetailsView : boolean = false;
  selectedRestaurant :any;	
  restaurants : any = [];
  matched : number = 0;
  matchedIngredients : any = [];


  constructor(public navCtrl: NavController, public navParams: NavParams,private call: CallNumber,private resData: CategoryProvider) {
  		this.selectedItems = navParams.get('selectedItems');
  }

  ionViewDidLoad() {
    this.selectedItems = this.navParams.get('selectedItems')?this.navParams.get('selectedItems'):'';
   
      this.resData.getRestaurants(this.selectedItems).subscribe((res => {
        if(res.data.length){
          this.restaurants = res.data;
          if(this.selectedItems.length){
            this.restaurants.forEach((element,key) => {
              this.selectedItems.forEach(ingredient => {
                if(element.ingredients.includes(ingredient.annotation)){
                    this.matched+=1;
                }
              });
              this.restaurants[key].matched = Math.round((this.matched/this.selectedItems.length)*100);
              this.matched=0;
            });  
          }
        }
        else{
          this.restaurants = [];
        }
         
        console.log(res.data);
      }));
     
      
    
   
  }      

  showSeleted(){
    this.navCtrl.push(LandingPage,{'selectedItems':this.selectedItems});
  }

  removeItem(index){
    this.selectedItems.splice(index,1);
    this.ionViewDidLoad();
  }

  showDetails(rest){
  	this.showDetailsView = true;
    this.selectedRestaurant = rest;
    this.matchedIngredients = this.selectedRestaurant.ingredients.split(',');
    this.selectedRestaurant.ingredients = this.matchedIngredients;
  }
  showRestaurants(){
  	this.showDetailsView = false;
  }
  makeCall(){
  	this.call.callNumber("919030070213", true)
  .then(() => console.log('Launched dialer!'))
  .catch(() => console.log('Error launching dialer'));
  }
}
