import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import { CategoryProvider } from '../../providers/category/category';
import { DeviceinfoProvider } from '../../providers/deviceinfo/deviceinfo';
import { Geolocation } from '@ionic-native/geolocation';
import { FilterPipe } from '../../pipes/filter/filter';
import { RestaurantsPage } from '../restaurants/restaurants';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import {} from '@types/googlemaps';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';



@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})

export class LandingPage {
	items : any =[];
	count = 0;
  selectedItems : any = [];
  watchId : any;
  currentPos : any;
  tempItems : any = [];
  myLocation : any;
  message : any;
  showSelected : boolean = false;
  searchText : string= '';
  options :any;
  geocoder : any;
  zone : any;
  nearbyItems : any;
  nextPageData : any = [];
  constructor(private platform: Platform, public navCtrl: NavController, public navParams: NavParams,public getData :CategoryProvider,public geo : Geolocation, private nativeGeocoder: NativeGeocoder, public alertMessage:AlertController,public loading: LoadingController) {
    this.geocoder = new google.maps.Geocoder();
    platform.ready().then(() => {
      
            // get current position
            geo.getCurrentPosition().then(pos => {
             
              console.log(pos ,  'lon: ' + pos.coords.longitude);
            });
      
            const watch = geo.watchPosition().subscribe(pos => {
              console.log('lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude);
            });
      
            // to stop watching
            watch.unsubscribe();
      
          });
  }
  ionViewDidLoad() {
    this.selectedItems = this.navParams.get('selectedItems')?this.navParams.get('selectedItems'):[];
    if(this.selectedItems.length>0)   
       this.showSelectedItems();
    else{
      this.getData.getCategories(this.count).subscribe((res => {
        this.items = res.data;   
        this.count = this.items.length;
        
      // this.nativeGeocoder.forwardGeocode('Berlin')
      //   .then((coordinates: NativeGeocoderForwardResult) => console.log('The coordinates are latitude=' + coordinates.latitude + ' and longitude=' + coordinates.longitude))
      //   .catch((error: any) => console.log(error));
      //  this.loadMap();

      }));
       
      this.nativeGeocoder.reverseGeocode(17.4530129,78.4327117)
      .then((result: NativeGeocoderReverseResult) => console.log(JSON.stringify(result)))
      .catch((error: any) => console.log(error));
     
      
    }
   
  }

  

    selectSearchResult(item){
    
      this.geocoder.geocode({'placeId': item.place_id}, (results, status) => {
        if(status === 'OK' && results[0]){
          this.geocoder.nearbySearch({
            location: results[0].geometry.location,
            radius: '500',
            types: ['restaurant'],
            key: 'AIzaSyCF6fsUcBfUr_RVeXJVycUhUURF_Vrhk3g'
          }, (near_places) => {
              this.zone.run(() => {
                this.nearbyItems = [];
                for (var i = 0; i < near_places.length; i++) {
                  this.nearbyItems.push(near_places[i]);
                }
            });
          })
        }
      })
      console.log(this.nearbyItems);
    }
  

    doInfinite(infiniteScroll){
      this.getData.getCategories(this.count).subscribe((res => {
        if(res.data){
       this.nextPageData = res.data;
       res.data.forEach(element => {
         this.items.push(element);
       });
      }
        this.count = this.items.length;
        infiniteScroll.complete();
      }));
  }
    

  findMyLocation(){
   
    if(this.selectedItems.length==0){
        let alert = this.alertMessage.create({
          subTitle: 'Please select at least one ingredient',
          buttons: ['OK']
        });
        alert.present();
        // let geoOptions = {enableHighAccuracy : true, timeout: 5000,
        //   maximumAge: 0};
        // this.currentPos = navigator.geolocation.getCurrentPosition(this.success,this.error,geoOptions);
       return;
    }

    let geoOptions = {enableHighAccuracy : true, timeout: 5000,
      maximumAge: 0};
    try{
      this.watchId = this.geo.watchPosition(geoOptions).subscribe(data=>{
        this.myLocation = data.coords.latitude;
       
      },
      error=>{
        this.message = "GPS error ";
      }
      );
      

    }catch(err){
        alert('error '+err)
        this.message = "error";
    }
    this.navCtrl.push(RestaurantsPage,{'selectedItems':this.selectedItems});
  }

   success(pos) {
    var crd = pos.coords;
  
    console.log('Your current position is:');
    console.log(`Latitude : ${crd.latitude}`);
    console.log(`Longitude: ${crd.longitude}`);
    console.log(`More or less ${crd.accuracy} meters.`);
  }
  error(){
    console.log("error");
  }

 showSelectedItems(){
    if(this.selectedItems.length>0){
      if(this.showSelected){
         this.showSelected = false;
         this.items = this.tempItems;
      }
        
      else{
        this.tempItems = this.items;
        this.items = this.selectedItems;
        this.showSelected = true ;
      }
    }
  }

  selectItem(item){

    if(item.selected){
       item.selected =false;
       var index = this.selectedItems.findIndex(x=>x.title === item.title)
      this.selectedItems.splice(index, 1);
    }
    else{
      item.selected = true;
      this.selectedItems.push(item);
    }
    
  }

  search(event){
    if(this.searchText.length>2){
      this.getData.getSearchdata(this.searchText).subscribe((res => {
        this.items = res.data;
        console.log(res.data);
      }));
    }
  }

  skipSelection(){
    this.selectedItems= [];
    this.navCtrl.push(RestaurantsPage,{'selectedItems':this.selectedItems});
  }

};