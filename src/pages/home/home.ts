import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LandingPage } from '../landing/landing';
import { Device } from '@ionic-native/device';
import { DeviceinfoProvider } from '../../providers/deviceinfo/deviceinfo';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  slider : any =[];
  constructor(public navCtrl: NavController, private device :Device, private deviceInfo : DeviceinfoProvider) {
  	this.slider=[
  	{
  		src:"assets/simages/swiper-img-1.png",
  		id:"1",
  		title:"Ingredient Selection",
  		desc : "Choose Ingredients you want to eat"
  	},
  	{
  		src:"assets/simages/swiper-img-2.png",
  		id:"2",
  		title:"Location:",
  		desc : "Activate your location"
  	},
    {
      src:"assets/simages/swiper-img-3.png",
      id:"2",
      title:"Ingredients Search",
      desc : "We find the match of receipes with ingredients you choose."
    },
		{
      src:"assets/simages/swiper-img-3.png",
      id:"2",
      title:"Call Resturant",
      desc : "Call the resturant and book a table!!"
    }

  	
    ];
    
    this.deviceInfo.saveDeviceId(this.device.uuid).subscribe((res)=>{console.log(res)});

  }

  goLanding(){
  	this.navCtrl.push(LandingPage);
  }
  

}
