
import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
 name: 'filter'
})

@Injectable()
export class FilterPipe implements PipeTransform {
 transform(items: any, searchText: any): any {
         if(searchText == null) return items;

        return items.filter(function(item){
          return item.annotation.toLowerCase().indexOf(searchText) > -1;
        })
      }
}