<?php
    require('connection.php');
    header("Content-Type:application/json");
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
 
    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }


    $data = json_decode($_POST['name']);
    $target_dir = "../resumes/";
    $target_file = $target_dir . basename($_FILES["file"]["name"]);
    move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
    $name = $data->username;
    $email = $data->email;
    $phone = $data->phone;
    $role = $data->role;
    $skills = $data->skills;
    $employed = $data->employed;
    $cctc = $data->cctc;
    $ectc = $data->ectc;
    $experience = $data->experience;
    $availability = $data->date;
    $linkedin_id = $data->linkedin_id;
    $resume_url = substr($target_file,3);
    $query = "INSERT INTO user (name, email, phone,role,skills,employed,cctc,ectc,experience,availability,linkedin_id,resume_url)
    VALUES ('$name', '$email', '$phone','$role','$skills','$employed','$cctc','$ectc','$experience','$availability','$linkedin_id','$resume_url')";
    if ($conn->query($query) === TRUE) {
        $to = $email;
        $subject = "Confirmation from Acenblu";
        $txt = "Thank you for registering with us..!";
        $headers = "From: acenblu.com" . "\r\n" .
        "CC: info@acenblu.com";
        
        mail($to,$subject,$txt,$headers);
        echo json_encode(array('status'=>'success','message'=>'Registrations Successful'));
    } else {
        echo json_encode(array('status'=>'failure','message'=>'Registrations Failed'));
    }
    
    $conn->close();
?>