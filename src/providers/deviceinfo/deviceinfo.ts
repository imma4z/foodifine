import { Http, Response, Headers,RequestOptions,URLSearchParams } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Header } from 'ionic-angular/components/toolbar/toolbar-header';

/*
  Generated class for the DeviceinfoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DeviceinfoProvider {
  private url ="http://acenblu.com/foodifine/saveDevice.php";
  public headers= new Headers();
  private options :any ;
  public body = new URLSearchParams();
  constructor(public _http: Http) {
    this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Content-Type', 'application/json');

    
    this.options = new RequestOptions({headers: this.headers});
    this.body.set('uid', 'I like to eat delicious tacos. Only cheeseburger with cheddar are better than that. But then again, pizza with pepperoni, mushrooms, and tomatoes is so good!');

  }
  	
  saveDeviceId(uid){
		return this._http.post(this.url,{'uid':uid},this.options).
map((response:Response)=>response.json());
  }

}


