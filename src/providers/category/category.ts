import { Http, Response, Headers,RequestOptions,URLSearchParams } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Header } from 'ionic-angular/components/toolbar/toolbar-header';



@Injectable()
export class CategoryProvider {
		private url : string = "http://acenblu.com/foodifine/index.php";
		private searchUrl : string = "http://acenblu.com/foodifine/getSearchData.php";
		private getRestaurantsUrl : string = "http://acenblu.com/foodifine/getRestaurants.php";
		private options :any ;
		public headers = new Headers();
		public body = new URLSearchParams();
		constructor(private _http : Http){	       
			this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
			this.headers.append('Accept', 'application/json');
			this.headers.append('Content-Type', 'application/json');
		
			
			this.options = new RequestOptions({headers: this.headers});
		}

	getCategories(count){
		return this._http.get(this.url+'?count='+count).
		map((response:Response)=>response.json());
	}

	getSearchdata(searchText){
		return this._http.get(this.searchUrl+'?searchText='+searchText).
		map((response:Response)=>response.json());
	}
	getRestaurants(postData){
		return this._http.post(this.getRestaurantsUrl,postData,this.options).
		map((response:Response)=>response.json());
	}
	 
    
}
